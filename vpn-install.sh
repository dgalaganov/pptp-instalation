apt-get update && 
apt-get remove --auto-remove nftables -y && 
apt-get purge nftables -y && 
apt-get install pptpd iptables -y && 
echo -e 'ms-dns 8.8.8.8\nms-dns 8.8.4.4' >> /etc/ppp/pptpd-options && 
echo -e 'extuser * q1ySpTXc6xOr *' >> /etc/ppp/chap-secrets && 
echo -e 'localip 10.0.0.1\nremoteip 10.0.0.100-200' >> /etc/pptpd.conf && 
echo -e 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf && 
sysctl -p && 
iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE && 
echo -e 'iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE' >> /etc/rc.local && 
service pptpd start && 
systemctl enable pptpd